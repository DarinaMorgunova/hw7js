// 1.Це функція , яка використовується для циклічного перебору масиву і виконання певної дії над кожним елементом масиву.
// 2.Використання методу splice(). Присвоєння змінній нового пустого масиву.
// 3. За допомогою методу Array.isArray().

function filterBy(arr, dataType) {
    if (!Array.isArray(arr)) {
      throw new Error('Перший аргумент - масив.');
    }
    if (typeof dataType !== 'string') {
      throw new Error('Другий аргумент - тип даних.');
    }
    return arr.filter(function(item) {
      return typeof item !== dataType;
    });
  }
  let arr = ['hello', 'world', 23, '23', null];
  let filteredArr = filterBy(arr, 'string');
  console.log(filteredArr); 